/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.gui.controler;

import io.kamax.grid.soler.core.State;
import io.kamax.grid.soler.gui.fx.main.MainGUI;
import javafx.application.Platform;
import javafx.stage.Stage;

public class MainController {

    public static void run(State state, Stage stage, MainGUI gui) {
        new MainController(state, stage, gui).show();
    }

    private State state;
    private Stage stage;
    private MainGUI gui;

    private MainController(State state, Stage stage, MainGUI gui) {
        this.state = state;
        this.stage = stage;
        this.gui = gui;
    }

    private void show() {
        Platform.runLater(() -> stage.setScene(gui.getMain()));
    }

}
