/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.gui.fx.auth.login;

import com.jfoenix.controls.JFXButton;
import io.kamax.grid.soler.gui.controler.LoginChoices;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

import java.util.function.Consumer;

public class LoginUIAuthSelectorPane extends GridPane {

    private Label choiceLabel;

    public LoginUIAuthSelectorPane() {
        choiceLabel = new Label("Choose your login method:");
        add(choiceLabel, 0, 0, Integer.MAX_VALUE, 1);
    }

    public void init(LoginChoices choices, Consumer<LoginChoices.Item> consumer) {
        choices.getCategories().forEach(cat -> {
            FlowPane p = new FlowPane();
            cat.getItems().forEach(i -> {
                JFXButton button = new JFXButton(i.getLabel());
                button.setOnAction(o -> consumer.accept(i));
                p.getChildren().add(button);
            });

            TitledPane catPane = new TitledPane(cat.getLabel(), p);
            catPane.setCollapsible(false);
            catPane.setExpanded(true);

            add(catPane, 0, 1, Integer.MAX_VALUE, 1);
        });
    }

}
