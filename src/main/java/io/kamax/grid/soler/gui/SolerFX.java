/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.gui;

import com.google.gson.JsonObject;
import io.kamax.grid.soler.api.GsonUtil;
import io.kamax.grid.soler.core.SolerHttpClient;
import io.kamax.grid.soler.core.State;
import io.kamax.grid.soler.gui.controler.LoginController;
import io.kamax.grid.soler.gui.controler.MainController;
import io.kamax.grid.soler.gui.fx.auth.login.AuthGUI;
import io.kamax.grid.soler.gui.fx.main.MainGUI;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class SolerFX extends Application {

    private static final Logger log = LoggerFactory.getLogger(SolerFX.class);

    public static void main(String[] args) {
        SolerFX.launch(args);
    }

    public Optional<State> load() {
        return Optional.empty();
    }

    public void save(State state) {
        JsonObject isConf = new JsonObject();
        isConf.addProperty("endpoint", state.getIsClient().getEndpoint());
        isConf.addProperty("token", state.getIsClient().getAccessToken());

        JsonObject conf = new JsonObject();
        conf.add("is", isConf);

        Path confDir = Path.of(System.getProperty("user.home"), ".config", "soler");
        try {
            if (Files.notExists(confDir)) {
                Files.createDirectories(confDir);
            }

            Path confFile = confDir.resolve("access.json");
            try (OutputStream os = new FileOutputStream(confFile.toFile())) {
                IOUtils.write(GsonUtil.getPretty().toJson(conf) + System.lineSeparator(), os, StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Soler");
        stage.setHeight(768);
        stage.setWidth(1024);

        Optional<State> oldState = load();
        if (oldState.isEmpty()) {
            log.info("No previous state found, starting fresh");
            State state = new State();
            LoginController.run(state, stage, new SolerHttpClient(), new AuthGUI(), () -> {
                save(state);
                MainController.run(state, stage, new MainGUI());
            });
        } else {
            MainController.run(oldState.get(), stage, new MainGUI());
        }

        stage.show();
    }

}
